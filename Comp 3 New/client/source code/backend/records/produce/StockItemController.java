package records.produce;

import utils.files.FileUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class StockItemController {

    private static Set<StockItem> itemHashSet = new HashSet<>();

    public static StockItem createNew(String name, long cost, int stock)
    {
        StockItem si = new StockItem(name, cost, stock);
        itemHashSet.add(si);
        flushChangesToDisk();
        return si;
    }

    public static void delete(StockItem toRemove)
    {
        itemHashSet.remove(toRemove);
    }

    public static Optional<StockItem> getByKeyfield(String keyfield)
    {
        for(StockItem si : itemHashSet)
        {
            if(si.getKeyField().equals(keyfield))
            {
                return Optional.of(si);
            }
        }
        return Optional.empty();
    }

    public static void flushChangesToDisk()
    {
        for(String currentPath : FileUtils.getFileNamesRecursively("data"))
        {
            if(!currentPath.endsWith(".stock")){continue;}
            FileUtils.deleteFile(currentPath);
        }
        for(StockItem si : itemHashSet)
        {
            FileUtils.writeObject(si,"stock");
        }
    }

    public static void loadFromDisk()
    {
        itemHashSet.clear();
        ArrayList<String> paths = FileUtils.getFileNamesRecursively("data");
        if(paths == null || paths.isEmpty())
        {
            return;
        }
        for(String currentPath : paths)
        {
            if(currentPath.endsWith(".stock"))
            {
                itemHashSet.add(FileUtils.<StockItem>readObject(currentPath).get());
            }
        }
    }

    public static HashSet<StockItem> getCopyOfStockItems()
    {
        HashSet<StockItem> copy = new HashSet<>();
        copy.addAll(itemHashSet);
        return copy;
    }

}
