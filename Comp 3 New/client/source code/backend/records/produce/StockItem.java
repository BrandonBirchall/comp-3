package records.produce;

import utils.files.Record;
import utils.Hash;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Optional;

/**
 * ----------------
 * component 3
 * Brandon Birchall
 * ----------------
 * Created by Brandon on 24/10/2017.
 * Don't Copy this without my permission!
 * Feel free to use for ideas, or to learn from.
 */
public class StockItem implements Record{

	static final long serialVersionUID = 1000L;

	private String keyField;
	private String name;
	private int stock;
	private long cost;




	//CONSTRUCTORS




	protected StockItem(String name, long cost, int stock) {
		this.cost = cost;
		this.name = name;
		this.stock = stock;

		this.keyField = Hash.simpleHash(this.toString());
	}




	//GETTERS




	public String getName() {
		return name;
	}

	public long getCost() {
		return cost;
	}

	@Override
	public String getKeyField() {
		return keyField;
	}

	public int getStock() {
		return stock;
	}





	//SETTERS




	public void setName(String name)
	{
		this.name = name;
		this.keyField = Hash.simpleHash(this.toString());
	}

	public void setCost(long cost)
	{
		this.cost = cost;
		this.keyField = Hash.simpleHash(this.toString());
	}

	public void setStock(int stock)
	{
		this.stock = stock;
		this.keyField = Hash.simpleHash(this.toString());
	}






	//MISC





	public void updateStock(int delta) {
		this.stock+=delta;
	}

	@Override
	public void writeObject(ObjectOutputStream oos) throws IOException {
		oos.defaultWriteObject();
		oos.writeObject(name);
		oos.writeLong(cost);
		oos.writeInt(stock);
	}

	@Override
	public void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
		ois.defaultReadObject();
		this.name = (String) ois.readObject();
		this.cost = ois.readLong();
		this.stock = ois.readInt();
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(keyField);
		sb.append(":");
		sb.append(name);
		sb.append(":");
		sb.append(cost);
		sb.append(":");
		sb.append(stock);
		return sb.toString();
	}
}
