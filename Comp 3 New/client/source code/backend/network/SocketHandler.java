package backend.network;

import java.io.*;

import java.util.concurrent.*;
import java.net.Socket;
import java.util.Scanner;

public class SocketHandler implements Runnable {

    protected Socket mySocket;

    public SocketHandler(Socket theSocket)
    {
        mySocket = theSocket;
	}
	
	@Override
	public void run()
	{
		ExecutorService messageListener = Executors.newSingleThreadExecutor();
		System.out.println("New connection to " + mySocket.getInetAddress());
		try {
			PrintWriter pw = new PrintWriter (mySocket.getOutputStream(), true);
			BufferedReader br = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
			Scanner s = new Scanner(System.in);
			messageListener.submit(() -> {
				while(true)
				{
					String message = null;
					try {
						message = br.readLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
					if(message==null || message.equals("null"))
					{
						try {
							mySocket.close();
							br.close();
							pw.close();
							break;
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					System.out.println("'" + message + "' received from " + mySocket.getInetAddress());
				}
				return;
			});
			while(true)
			{

				String message = s.nextLine();
				if(message.equalsIgnoreCase("quit"))
				{
					mySocket.close();
					break;
				}
				pw.println(message);
				//System.out.println("'" + "this is a test message" + "' sent to " + mySocket.getInetAddress());
			}
			messageListener.shutdownNow();
			pw.close();
			br.close();
			s.close();
			} catch (IOException e) {
			e.printStackTrace();

		}
		System.out.println("Disconnecting from " + mySocket.getInetAddress());
	}
	
    //public abstract void stop();
}
