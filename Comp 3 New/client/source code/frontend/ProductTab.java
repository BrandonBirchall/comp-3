package frontend;

class ProductTab extends AbstractTab
{
	private static ProductTab instance;
	private ProductTab()
	{
		super();
		this.setText("Product List");
	}
	public static ProductTab getInstance()
	{
		if(instance==null)
		{
			instance = new ProductTab();
		}
		return instance;
	}

	@Override
	public void build()
	{
		
	}
}