package frontend;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
abstract class AbstractTab extends Tab
{

	AbstractTab()
	{
		this.setContent(contentPane);
		build();
	}

	private Pane contentPane = new Pane();

	public abstract void build();

	
	//Buttons
	
	protected Button addButton(String text, double xPos, double yPos, String eventName)
	{
		Button buttonToAdd = new Button();
		buttonToAdd.setText(text);
		buttonToAdd.setLayoutX(xPos);
		buttonToAdd.setLayoutY(yPos);
		
		/*All components in the below arraylist
		  Are loaded into the tab once the
		  addComponents() method is called*/
		add(buttonToAdd);
		
		//Add to eventbus.
		return buttonToAdd;
	}
	
	
	
	//Labels
	
	protected Label addLabel(String text, double xPos, double yPos)
	{
		Label labelToAdd = new Label();
		labelToAdd.setText(text);
		labelToAdd.setLayoutX(xPos);
		labelToAdd.setLayoutY(yPos);
		
	    /*All components in the below arraylist
		Are loaded into the tab once the
		addComponents() method is called*/
		add(labelToAdd);

		return labelToAdd;
	}
	
	
	
	//Textfields
	
	protected TextField addTextField(double xPos, double yPos)
	{
		TextField tfToAdd = new TextField();
		
		tfToAdd.setLayoutX(xPos);
		tfToAdd.setLayoutY(yPos);
		
		/*All components in the below arraylist
		Are loaded into the tab once the
		addComponents() method is called*/
		add(tfToAdd);
		
		return tfToAdd;
	}
	
	protected TextField addTextField(double xPos, double yPos, String placeholderText)
	{
		TextField tf = addTextField(xPos,yPos);
		tf.setText(placeholderText);
		return tf;
	}

	
	
	//Tables
	
	protected TableView addTable(double xPos, double yPos)
	{
		TableView tableToAdd = new TableView();
		
		tableToAdd.setLayoutX(xPos);
		tableToAdd.setLayoutY(yPos);
		
		/*All components in the below arraylist
		Are loaded into the tab once the
		addComponents() method is called*/
		add(tableToAdd);
		
		return tableToAdd;
	}	
	
	protected TableView addTable(double xPos, double yPos, TableColumn column)
	{
		TableView tableToAdd = new TableView();
		
		tableToAdd.setLayoutX(xPos);
		tableToAdd.setLayoutY(yPos);

		tableToAdd.getColumns().add(column);
		
		/*All components in the below arraylist
		Are loaded into the tab once the
		addComponents() method is called*/
		add(tableToAdd);
		
		return tableToAdd;
	}

	//Text Area

	protected TextArea addTextArea(double xPos, double yPos)
	{
		TextArea tfToAdd = new TextArea();

		tfToAdd.setLayoutX(xPos);
		tfToAdd.setLayoutY(yPos);

		/*All components in the below arraylist
		Are loaded into the tab once the
		addComponents() method is called*/
		add(tfToAdd);

		return tfToAdd;
	}

	protected TextArea addTextArea(double xPos, double yPos, String placeholderText)
	{
		TextArea tf = addTextArea(xPos,yPos);
		tf.setText(placeholderText);
		return tf;
	}

	protected void add(Node toAdd)
	{
		contentPane.getChildren().add(toAdd);
	}
	
	

}