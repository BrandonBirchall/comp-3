package frontend;

/**
 * ----------------
 * component 3
 * Brandon Birchall
 * ----------------
 * Created by Brandon on 13/11/2017.
 * Don't Copy this without my permission!
 * Feel free to use for ideas, or to learn from.
 */
public class AbstractTabFactory {

	/*

	An abstract factory is used here to allow all concrete tab implementations
	to be package-private. This decouples external classes and hides the implementation
	and functionality of these concrete classes to anything outside the package.

	 */

	public AbstractTab buildAdminTab(){
		return AdminTab.getInstance();
	}
	
	public AbstractTab buildCommsTab(){
		return CommsTab.getInstance();
	}
	
	public AbstractTab buildProductTab(){
		return ProductTab.getInstance();
	}
	
	public AbstractTab buildStocksTab(){
		return StocksTab.getInstance();
	}
	
	public AbstractTab buildTaskViewTab(){
		return TaskViewTab.getInstance();
	}
	
	public AbstractTab buildTillTab(){
		return TillTab.getInstance();
	}

}
