package frontend;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class GUIProcess extends Application implements Runnable
{
	private static final int WIDTH=1280,HEIGHT=720;

	private Scene theScene;
	public GUIProcess(){
	}

	@Override
	public void run()
	{
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		TabPane thePane = new TabPane();
		AbstractTabFactory abstractTabFactory = new AbstractTabFactory();

		//EmployeeController.addEmployee(5,"brandon",50);

		thePane.getTabs().add(abstractTabFactory.buildProductTab());
		thePane.getTabs().add(abstractTabFactory.buildStocksTab());
		thePane.getTabs().add(abstractTabFactory.buildAdminTab());
		thePane.getTabs().add(abstractTabFactory.buildCommsTab());
		thePane.getTabs().add(abstractTabFactory.buildTaskViewTab());
		thePane.getTabs().add(abstractTabFactory.buildTillTab());

		theScene = new Scene(thePane);

		//theScene.getRoot();  //pane

		primaryStage.setScene(theScene);
		primaryStage.setTitle("Comp 3 Prototype");
		primaryStage.setWidth(WIDTH);
		primaryStage.setHeight(HEIGHT);

		primaryStage.show();

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				//ProductController.getInstance().storeProductsInFile();
				//StocksController.getInstance().storeStockItemsInFile();
				//ServerThread.stop();
				Platform.exit();
			}
		});
	}

	public static void main(String[] args) {
		GUIProcess gp = new GUIProcess();
		gp.run();
	}
}