package frontend;

/**
 * ----------------
 * component 3
 * Brandon Birchall
 * ----------------
 * Created by Brandon on 14/11/2017.
 * Don't Copy this without my permission!
 * Feel free to use for ideas, or to learn from.
 */
class TillTab extends AbstractTab {

	private static TillTab instance;
	private TillTab()
	{
		super();
		setText("Tills");
	}
	public static TillTab getInstance()
	{
		if(instance==null)
		{
			instance = new TillTab();
		}
		return instance;
	}


	@Override
	public void build() {

	}
}
