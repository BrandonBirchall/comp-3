package frontend;


import javafx.scene.control.TableView;

class StocksTab extends AbstractTab
{

	private static StocksTab instance;
	private StocksTab()
	{
		super();
		this.setText("Stock List");
	}
	public static StocksTab getInstance()
	{
		if(instance==null)
		{
			instance = new StocksTab();
		}
		return instance;
	}

	@Override
	public void build()
	{
		
	}
}