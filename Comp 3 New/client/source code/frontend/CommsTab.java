package frontend;



import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.util.Date;

class CommsTab extends AbstractTab
{
	//private Employee currentEmployee = new Employee(0,"placeholder", 5.50);
	private static CommsTab instance;
	private CommsTab()
	{
		super();
		this.setText("Communications");
	}
	public static CommsTab getInstance()
	{
		if(instance==null)
		{
			instance = new CommsTab();
		}
		return instance;
	}

	@Override
	public void build()
	{
		
	}
}