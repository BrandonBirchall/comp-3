package frontend;
class TaskViewTab extends AbstractTab
{
	private static TaskViewTab instance;
	private TaskViewTab()
	{
		super();
		setText("Tasks");
	}
	public static TaskViewTab getInstance()
	{
		if(instance==null)
		{
			instance = new TaskViewTab();
		}
		return instance;
	}

	@Override
	public void build()
	{
		addButton("Test button", 0,50,"Test");
	}
}