package frontend;
class AdminTab extends AbstractTab
{
	private static AdminTab instance;
	
	private AdminTab(){
		super();
		this.setText("Admin");
	}
	public static AdminTab getInstance(){
		if(instance==null)
		{
			instance = new AdminTab();
		}
		return instance;
	}

	@Override
	public void build(){
	
	}
}