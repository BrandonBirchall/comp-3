package utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

/**
 * ----------------
 * component 3
 * Brandon Birchall
 * ----------------
 * Created by Brandon on 29/10/2017.
 * Don't Copy this without my permission!
 * Feel free to use for ideas, or to learn from.
 */

public class Hash {
	public static String simpleHash(String toHash)
	{
		int hash = toHash.length();
		for(int i = 0; i < toHash.length(); i++)
		{
			hash = ((hash << 5) ^ (hash >> 27)) ^ toHash.charAt(i);
		}
		return Integer.toHexString(hash).toUpperCase();
	}

	public static Optional<String> keccak(String toHash)
	{
		try {
			MessageDigest md = MessageDigest.getInstance("SHA3-256");
			return Optional.of(new String(md.digest(toHash.getBytes())));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}
}
