package utils.files;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public interface Record extends Serializable{
    String getKeyField();
    void writeObject(ObjectOutputStream objectOutputStream) throws IOException;
    void readObject(ObjectInputStream objectInputStream) throws ClassNotFoundException, IOException;
}
