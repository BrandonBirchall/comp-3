package utils;

import java.util.ArrayList;

public interface Observable {

    ArrayList<Listener> listeners = new ArrayList<>();

    default void addListener(Listener toAdd)
    {
        listeners.add(toAdd);
    }
    default void removeListener(Listener toAdd)
    {
        listeners.remove(toAdd);
    }
    default void notifyListeners(String message)
    {
        listeners.forEach(i -> i.notify(message));
    }
}
