package utils.files;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;

public class FileUtils {
	private FileUtils() {
	}

	public static ArrayList<String> getFileNamesRecursively(String directory) {
		ArrayList<String> files = new ArrayList<>();
		File theDir = new File(directory);
		if (theDir.listFiles() == null) {
			return null;
		}
		//Arrays.stream(theDir.listFiles()).forEach(e->files.add(e.getPath()));
		for (File a : theDir.listFiles()) {
			if (a.isDirectory()) {
				files.addAll(getFileNamesRecursively(a.getPath()));
			} else {
				files.add(a.getPath());
			}
		}
		return files;
	}

	public static void deleteFile(String filename) {
		System.out.println("Deleting " + filename);
		File file = new File(filename);
		if (file.delete()) {
		} else {
			System.err.println("Failed to delete file " + file.getName());
		}
	}

	public static <T extends Record> void writeObject(T toWrite, String extension) {
		try {
			File f = new File("data/" + toWrite.getKeyField() + "." + extension);
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(toWrite);
			oos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static <T extends Record> Optional<T> readObject(String path) {
		Optional<T> tOptional = Optional.empty();
		try {
			FileInputStream fileIn = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			tOptional = Optional.of((T) in.readObject());
			in.close();
			fileIn.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return tOptional;
	}
}