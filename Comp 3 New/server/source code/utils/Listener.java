package utils;

public interface Listener {
    void notify(String message);
}
