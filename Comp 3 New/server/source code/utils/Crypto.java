package utils;

import com.sun.org.apache.xml.internal.security.utils.Base64;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Optional;

public class Crypto {
    public static String simpleHash(String toHash)
    {
        int hash = toHash.length();
        for(int i = 0; i < toHash.length(); i++)
        {
            hash = ((hash << 5) ^ (hash >> 27)) ^ toHash.charAt(i);
        }
        return Integer.toHexString(hash).toUpperCase();
    }

    public static Optional<String> keccak(String toHash)
    {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA3-256");
            return Optional.of(new String(md.digest(toHash.getBytes())));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static String bytesToHex(byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for(byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }
    public static byte[] hexToBytes(String s){
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private static Cipher getMutual() throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        return cipher;// cipher.doFinal(pass.getBytes());
    }

    public static byte[] encrypt(String pass, SecretKeySpec sk) throws Exception {
        Cipher cipher = getMutual();
        cipher.init(Cipher.ENCRYPT_MODE, sk);
        byte[] encrypted = cipher.doFinal(pass.getBytes("UTF-8"));
        return encrypted;
    }

    public static byte[] encrypt(String pass, PublicKey pk) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pk);
        return cipher.doFinal(pass.getBytes("UTF-8"));
    }
    public static byte[] encrypt(byte[] pass, PublicKey pk) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pk);
        return cipher.doFinal(pass);
    }
    public static String decrypt(byte[] encrypted, PrivateKey sk) throws Exception
    {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, sk);
        return bytesToHex(cipher.doFinal(encrypted));
    }

    public static String decrypt(byte[] encrypted, SecretKeySpec sk) throws Exception {
        Cipher cipher = getMutual();
        cipher.init(Cipher.DECRYPT_MODE, sk);
        String realPass = new String(cipher.doFinal(encrypted));
        return realPass;
    }

    public static SecretKeySpec generateKey() throws NoSuchAlgorithmException {
        byte[] aesKey;
        SecretKeySpec aeskeySpec;
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(256);
        SecretKey key = kgen.generateKey();
        aesKey = key.getEncoded();
        aeskeySpec = new SecretKeySpec(aesKey, "AES");

        return aeskeySpec;
    }

    public static PublicKey getKey(String key){
        try{
            byte[] byteKey = hexToBytes(key);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            return kf.generatePublic(X509publicKey);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) throws Exception {
        SecretKeySpec sk = generateKey();
        byte[] enc = encrypt("Brandon", sk);
        System.out.println(bytesToHex(enc));
        System.out.println(bytesToHex(sk.getEncoded()));
        SecretKeySpec secretKeySpec = new SecretKeySpec(hexToBytes(bytesToHex(sk.getEncoded())), "AES");
        //System.out.println(decrypt(enc, secretKeySpec));
        System.out.println(decrypt(enc, sk));
    }
}
