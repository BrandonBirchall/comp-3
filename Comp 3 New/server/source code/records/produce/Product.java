package records.produce;/**
 * ----------------
 * component 3
 * Brandon Birchall
 * ----------------
 * Created by Brandon on 24/10/2017.
 * Don't Copy this without my permission!
 * Feel free to use for ideas, or to learn from.
 */

import network.Console;
import utils.Crypto;
import utils.files.Record;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Logger;

public class Product implements Record{

	//https://stackoverflow.com/questions/7290777/java-custom-serialization
	//https://howtodoinjava.com/core-java/serialization/custom-serialization-readobject-writeobject/
	
	private static final Logger LOGGER = Logger.getLogger(Product.class.getName());

	private String keyField;
	private String name;
	private String description;
	private long price;
	private ArrayList<StockItem> stockItems = new ArrayList<>();
	
	
	
	
	//CONSTRUCTORS
	
	
	
	
	Product(String name, String description, long price) {
		Console.info("-=-----------------=-");
		Console.info("Making new Product:");
		
		this.price = price;
		this.name = name;
		this.description = description;

		this.keyField = Crypto.simpleHash(this.toString());
		Console.info("	keyfield: " + keyField);
		Console.info("	price: " + price);
		Console.info("	name: " + name);
		Console.info("	description: " + description);
		Console.info("-=-----------------=-");
	}

	
	
	
	//GETTERS
	
	
	
	
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public long getPrice() {
		return price;
	}

	public String getKeyField() {
		return keyField;
	}

	@Override
	public void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
		objectOutputStream.writeObject(name);
		objectOutputStream.writeObject(description);
		objectOutputStream.writeLong(price);
		objectOutputStream.writeObject(stockItems);
	}

	@Override
	public void readObject(ObjectInputStream objectInputStream) throws ClassNotFoundException, IOException {
		this.name = (String) objectInputStream.readObject();
		this.description = (String) objectInputStream.readObject();
		this.price = objectInputStream.readLong();
		this.stockItems = (ArrayList<StockItem>) objectInputStream.readObject();
	}

	public ArrayList<StockItem> getStockItems() {
		ArrayList<StockItem> toReturn = new ArrayList<>();
		toReturn.addAll(stockItems);
		return toReturn;
	}

	public int getStock(){
		if(stockItems.size()==0)
		{
			return 0;
		}
		int currentMinimum = stockItems.get(0).getStock();
		int currentItemStock;
		for(StockItem stockItem : stockItems)
		{
			currentItemStock = stockItem.getStock();
			if(currentItemStock < currentMinimum)
			{
				currentMinimum = currentItemStock;
			}
		}
		return currentMinimum;
	}
	
	
	
	
	//SETTERS
	
	

	
	public void setName(String name)
	{
		this.name = name;
		this.keyField = Crypto.simpleHash(this.toString());
	}

	public void setDescription(String description)
	{
		this.description = description;
		this.keyField = Crypto.simpleHash(this.toString());
	}

	public void setCost(long cost)
	{
		this.price = cost;
		this.keyField = Crypto.simpleHash(this.toString());
	}




	//ACTIONS




	public void addStockItem(StockItem toAdd)
	{
		this.stockItems.add(toAdd);
	}

	public void updateStock(int delta)
	{
		this.stockItems.forEach(x->x.updateStock(delta));
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(keyField);
		sb.append(":");
		sb.append(name);
		sb.append(":");
		sb.append(description);
		sb.append(":");
		sb.append(price);
		sb.append(":");
		for(int i = 0; i <stockItems.size(); i++)
		{
			sb.append(stockItems.get(i).getKeyField());
			if(i!=stockItems.size()-1) sb.append(",");
		}
		return sb.toString();
	}
}
