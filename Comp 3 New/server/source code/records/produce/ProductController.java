package records.produce;

import network.Console;
import utils.files.FileUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class ProductController {
    private static Set<Product> itemHashSet = new HashSet<>();

    public static Product createNew(String name, String description, long price)
    {
        Product p = new Product(name, description, price);
        itemHashSet.add(p);
        return p;
    }

    public static Product createNew(String name, String description, long price, ArrayList<StockItem> stockItems)
    {
        Product p = new Product(name, description, price);
        stockItems.forEach(p::addStockItem);
        itemHashSet.add(p);
        flushChangesToDisk();
        return p;
    }

    public static void delete(Product toRemove)
    {
        itemHashSet.remove(toRemove);
    }

    public static Optional<Product> getByKeyfield(String keyfield)
    {
        for(Product p : itemHashSet)
        {
            if(p.getKeyField().equals(keyfield))
            {
                return Optional.of(p);
            }
        }
        return Optional.empty();
    }

    public static void flushChangesToDisk()
    {
        for(String currentPath : FileUtils.getFileNamesRecursively("data"))
        {
            if(!currentPath.endsWith(".product")){continue;}
            FileUtils.deleteFile(currentPath);
        }
        for(Product p : itemHashSet)
        {
            Console.info("writing product " + p.getKeyField() + " to disk");
            FileUtils.writeObject(p,"product");
        }
    }

    public static void loadFromDisk()
    {
        itemHashSet.clear();
        ArrayList<String> paths = FileUtils.getFileNamesRecursively("data");
        if(paths == null || paths.isEmpty())
        {
            return;
        }
        for(String currentPath : paths)
        {
            Console.info("reading " + currentPath);
            if(currentPath.endsWith(".product"))
            {
                itemHashSet.add(FileUtils.<Product>readObject(currentPath).get());
            }
        }
    }

    public static HashSet<Product> getCopyOfProducts()
    {
        HashSet<Product> copy = new HashSet<>();
        copy.addAll(itemHashSet);
        return copy;
    }
}
