package network;

import java.sql.Date;

public class Console {

    public static void info(String information)
    {
        System.out.println(new Date(System.currentTimeMillis()).toString() + " [INFO] :" + information);
    }
}
