package network;

import records.produce.Product;
import records.produce.ProductController;
import records.produce.StockItemController;

public class Main{

	private static ServerThread st;

	public static void main(String[] args)
	{
		Console.info("starting server on port " + args[0] + " with " + args[1] + " threads");
		ProductController.loadFromDisk();
		StockItemController.loadFromDisk();
		//Product p = ProductController.createNew("test", "test2", 123);
		//System.out.println(p.getKeyField());
		st = new ServerThread(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		st.run();
		StockItemController.flushChangesToDisk();
		ProductController.flushChangesToDisk();
		st.stop();
		Console.info("server stopped");
	}

	protected static void stopServer()
	{
		st.stop();
	}
}
