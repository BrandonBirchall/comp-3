package network;

import utils.Observable;
import java.util.Scanner;


public class ConsoleThread implements Runnable, Observable{



    @Override
    public void run() {
        String input;
        Scanner s = new Scanner(System.in);
        while(true)
        {
            input = s.nextLine().toLowerCase();
            String[] splitInput = input.split(" ");
            switch(splitInput[0])
            {
                case "stop":
                {
                    notifyListeners("shutdown");
                }
                case "kick":
                {
                    notifyListeners("kick:" + splitInput[1]);
                }
            }
        }
    }

}
