package network;

import network.sessions.Session;
import network.sessions.SessionController;
import network.sessions.SessionHandlerThread;
import utils.Crypto;

import java.io.*;
import java.net.*;
import java.security.PublicKey;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerThread implements Runnable{

    private final int PORT;
    private final int THREADS;
    private boolean running = true;

    private ExecutorService es;
    private ServerSocket ss;

    public ServerThread(int port, int threadAllocation)
    {
        this.PORT = port;
        this.THREADS = threadAllocation;
    }

    @Override
    public void run() {

        //Create a thread pool with <THREADS> threads.
		es = Executors.newFixedThreadPool(THREADS);

		//Create a console handler that deals with server-side commands
		ConsoleThread ct = new ConsoleThread();

		//Add some simple listeners
		ct.addListener(message -> {
		    if(message.equalsIgnoreCase("shutdown"))
            {
                Console.info("shutting down");
                stop();
            }
            if(message.contains("kick"))
            {
                String tokick = message.split(":")[1];
                try {

                } catch (Exception e) {
                    Console.info(e.getMessage());
                }
            }
        });


		//Run the console thread concurrently
        es.submit(ct);


        try {
            //Create the server thread
            ss = new ServerSocket(PORT);


            Console.info("server started");

            //Will the server should still be running...
            while(running)
            {
                Socket mySocket = ss.accept();

                PrintWriter pw;
                BufferedReader br;

                //Initialize Streams
                pw = new PrintWriter(mySocket.getOutputStream(), true);
                br = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));

                pw.println("req-pubkey");
                //Read the message
                String publicKey = br.readLine();

                //Break if null
                if(publicKey == null || publicKey.equalsIgnoreCase("null")) continue;

                PublicKey pk = Crypto.getKey(publicKey);
                Session s = SessionController.createNewSession(pk, mySocket);

                //... accept any connection and create a thread to handle it
                es.submit(new SessionHandlerThread(s));
            }
        } catch (IOException e) {
            Console.info(e.getMessage());
        }
    }

    public void stop()
    {
        //Prevent loop from executing again...
        running = false;

        //Tell threadpool to close dormant threads
        es.shutdown();

        //Close the server socket, terminating all current connections
        try {

            ss.close();
        } catch (IOException e) {
            Console.info(e.getMessage());
        }
    }

    //Loops accepting connections and creates new threads for new connections, Passing them to handlers
}
