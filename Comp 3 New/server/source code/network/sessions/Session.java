package network.sessions;


import utils.Crypto;
import utils.Observable;

import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.security.PublicKey;

public class Session{
    private Socket s;
    private PublicKey keyFile;

    Session(Socket s, PublicKey key) {
        this.s = s;
        this.keyFile = keyFile;

        try {
            is = getSocket().getInputStream();

            os = getSocket().getOutputStream();

            br = new BufferedReader(new InputStreamReader(is));
            pw = new PrintWriter(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return s;
    }

    private InputStream is;
    private OutputStream os;
    private PrintWriter pw;
    private BufferedReader br;


    public void writeString(String toWrite)
    {
        String message = "string:"+toWrite;
        SecretKeySpec keySpec;
        try {
            keySpec = Crypto.generateKey();
            byte[] encrypted = Crypto.encrypt(message,keySpec);
            pw.println(Crypto.bytesToHex(Crypto.encrypt(keySpec.getEncoded(), keyFile)));
            pw.println(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readString() throws IOException {
        return br.readLine();
    }
}
