package network.sessions;

import utils.Listener;

import java.io.IOException;

public class SessionHandlerThread implements Runnable{
    private Session session;
    private boolean running;

    public SessionHandlerThread(Session session) {
        this.session = session;
    }

    @Override
    public void run() {
        while(running)
        {
            try {
                String m = session.readString();
                System.out.println(m);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void stop()
    {
        running = false;
        try {
            session.getSocket().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
