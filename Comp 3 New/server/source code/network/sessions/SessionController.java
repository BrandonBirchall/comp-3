package network.sessions;

import java.net.Socket;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class SessionController {
    private static List<Session> sessions = new ArrayList<>();

    public static Session createNewSession(PublicKey key, Socket theSocket)
    {
        Session s = new Session(theSocket, key);
        sessions.add(s);
        return s;
    }

    public static void removeSession(Session toRemove)
    {
        sessions.remove(toRemove);
    }
}
